<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <style>
          .odd {
          background-color: #FFE4B5;
          }

          .even {
          background-color: #F5FFFA;
          }
        </style>
      </head>
      <body>
        <h2>Table of Decathlon Players</h2>
        <table border="1">
          <tr bgcolor="#9acd32">
            <th align="left" rowspan="2">No.</th>
            <th align="left" rowspan="2">Player</th>
            <th align="center" colspan="10">Results</th>
            <th align="left" rowspan="2">Total points</th>
            <th align="left" rowspan="2">Place</th>
            <tr>
              <th>Run100m (s)</th>
              <th>LongJump (cm)</th>
              <th>ShotPut (m)</th>
              <th>HighJump (cm)</th>
              <th>Run400m (s)</th>
              <th>Run110MHurdles (s)</th>
              <th>DiscusThrow (m)</th>
              <th>PoleVault (cm)</th>
              <th>JavelinThrow (m)</th>
              <th>Run1500m (mm.ss.mmm)</th>
            </tr>
          </tr>
          <xsl:for-each select="//person">
            <xsl:variable name="css-class">
              <xsl:choose>
                <xsl:when test="position() mod 2 = 0">even</xsl:when>
                <xsl:otherwise>odd</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <tr class="{$css-class}">
              <td>
                <xsl:value-of select="no" />
              </td>
              <td>
                <xsl:value-of select="name" />
              </td>
              <td>
                <xsl:value-of select="run_100_m" />
              </td>
              <td>
                <xsl:value-of select="long_jump" />
              </td>
              <td>
                <xsl:value-of select="shot_put" />
              </td>
              <td>
                <xsl:value-of select="high_jump" />
              </td>
              <td>
                <xsl:value-of select="run_400_m" />
              </td>
              <td>
                <xsl:value-of select="run_110_m_hurdles" />
              </td>
              <td>
                <xsl:value-of select="discus_throw" />
              </td>
              <td>
                <xsl:value-of select="pole_vault" />
              </td>
              <td>
                <xsl:value-of select="javelin_throw" />
              </td>
              <td>
                <xsl:value-of select="run_1500_m" />
              </td>
              <td>
                <xsl:value-of select="total_points" />
              </td>
              <td>
                <xsl:value-of select="place" />
              </td>
            </tr>
          </xsl:for-each>
        </table><br/>
        (m) - meters<br/>
        (s) - seconds<br/>
        (cm) - centimeters<br/>
        (mm.ss.mmm) - minutes.seconds.milliseconds
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>