package lt.swedbank.decathlon;

import lt.swedbank.decathlon.domain.Points;
import lt.swedbank.decathlon.domain.Result;
import lt.swedbank.decathlon.util.CMDArgumentReaderUtil;
import lt.swedbank.decathlon.util.CSVParserUtil;
import lt.swedbank.decathlon.util.ResultPointUtil;
import lt.swedbank.decathlon.util.XMLFormatterUtil;
import lt.swedbank.decathlon.util.dto.InputArgs;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Result results = new Result();
        Points[] pointsOfAllPlayers;
        InputArgs inputArgs = CMDArgumentReaderUtil.readArguments(args);

        if (inputArgs.getState() != 1) {

            File inputFile = inputArgs.getInputFile();
            File outputFile = inputArgs.getOutputFile();
            String[] parsedData = CSVParserUtil.getParsedOutput(inputFile);

            results.setResultArray(parsedData);

            pointsOfAllPlayers = ResultPointUtil
                    .calculatePointsByPlayer(results);

            int[] totalPointsOfAllPlayers = ResultPointUtil
                    .calculateTotalPointsOfAllPlayers(pointsOfAllPlayers);

            String[] places = ResultPointUtil
                    .calculatePlaces(totalPointsOfAllPlayers);

            String xmlFormattedOutput = XMLFormatterUtil
                    .sortAndFormat(totalPointsOfAllPlayers, places, parsedData);

            XMLFormatterUtil.writeToFile(outputFile, xmlFormattedOutput);

        }
    }
}