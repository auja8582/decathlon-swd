package lt.swedbank.decathlon.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Utility to help to create, sort, and write output to file as xml
 */
public class XMLFormatterUtil {
String lol;
    /**
     * The method sets ascending order in given arrays. Ascending order is
     * calculated by points array.
     *
     * @param points - an array of all players' total points
     * @param places - an array of players' places
     * @param data   - an array of data strings from which points were
     *               calculated
     */
    private static final Object[] sort(int[] points, String[] places,
                                       String[] data) {
        for (int i = 0; i < points.length - 1; i++) {
            for (int j = 0; j < points.length - 1 - i; j++) {
                if (points[j + 1] > points[j]) {
                    int temp = points[j];
                    points[j] = points[j + 1];
                    points[j + 1] = temp;

                    String temp2 = places[j];
                    places[j] = places[j + 1];
                    places[j + 1] = temp2;

                    String temp3 = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp3;
                }
            }
        }
        Object[] resultSet = new Object[3];
        resultSet[0] = points;
        resultSet[1] = places;
        resultSet[2] = data;
        return resultSet;
    }

    /**
     * The method returns xml like formatted output
     *
     * @param points - an array of all players' total points
     * @param places - an array of players' places
     * @param data   - an array of data strings from which points were
     *               calculated
     * TODO make string concatenation optimal
     */
    private static final String makeXmlOutput(int[] points, String[] places,
                                              String[] data) {
        String XMLOutput = "<?xml version = \"1.0\" encoding = \"UTF-8\"?>" +
                "\n";
        XMLOutput += "<?xml-stylesheet type=\"text/xsl\" " +
                "href=\"xslt_file.xsl\"?>\n";
        XMLOutput += "<data>\n";
        for (int i = 0; i < points.length; i++) {
            XMLOutput += "<person>\n";
            XMLOutput += "<no>" + (i + 1)
                    + "</no>" + "\n";
            XMLOutput += "<name>" + data[i].split(";", 2)[0]
                    + "</name>" + "\n";
            XMLOutput += "<run_100_m>" + data[i].split(";")[1]
                    + "</run_100_m>" + "\n";
            XMLOutput += "<long_jump>" + data[i].split(";")[2]
                    + "</long_jump>" + "\n";
            XMLOutput += "<shot_put>" + data[i].split(";")[3]
                    + "</shot_put>" + "\n";
            XMLOutput += "<high_jump>" + data[i].split(";")[4]
                    + "</high_jump>" + "\n";
            XMLOutput += "<run_400_m>" + data[i].split(";")[5]
                    + "</run_400_m>" + "\n";
            XMLOutput += "<run_110_m_hurdles>" + data[i].split(";")[6]
                    + "</run_110_m_hurdles>" + "\n";
            XMLOutput += "<discus_throw>" + data[i].split(";")[7]
                    + "</discus_throw>" + "\n";
            XMLOutput += "<pole_vault>" + data[i].split(";")[8]
                    + "</pole_vault>" + "\n";
            XMLOutput += "<javelin_throw>" + data[i].split(";")[9]
                    + "</javelin_throw>" + "\n";
            XMLOutput += "<run_1500_m>" + data[i].split(";")[10]
                    + "</run_1500_m>" + "\n";
            XMLOutput += "<total_points>" + points[i]
                    + "</total_points>" + "\n";
            XMLOutput += "<place>" + places[i] + "</place>" + "\n";
            XMLOutput += "</person>\n\n";

        }
        XMLOutput += "</data>";
        return XMLOutput;
    }

    public static final String sortAndFormat(int[] points, String[] places,
                                             String[] data) {
        Object[] resultSet = sort(points, places, data);
        return makeXmlOutput((int[]) resultSet[0], (String[]) resultSet[1],
                (String[]) resultSet[2]);
    }

    /**
     * The method writes given string to a given file.
     *
     * @param file    - a file to write to.
     * @param content - a string to write to file.
     * @return file - a file which contains written string.
     */

    public static final File writeToFile(File file, String content)
            throws IOException {

        if (!file.exists()) {
            file.createNewFile();
        }

        try (FileWriter fw = new FileWriter(file.getAbsoluteFile());
             BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(content);
        }
        return file;
    }

}
