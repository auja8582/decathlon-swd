package lt.swedbank.decathlon.util;

import lt.swedbank.decathlon.domain.Points;
import lt.swedbank.decathlon.domain.Result;

import java.util.*;

/**
 * Used for result tinkering and point calculations
 */
public class ResultPointUtil {
    /**
     * The method calculates/converts given result string to points
     *
     * @param resultString - event results for one person like distance or time
     * @return Points - points array object for one person.
     */
    private final Points presetPlayerPoints(String resultString) {
        Points playerPoints = new Points();
        double[] currentResult = new double[10];
        int[] playerPointsByEvents = new int[10];
        int index = 0;

        for (String result : resultString.trim().split(";")) {
            try {
                currentResult[index] = Double.parseDouble(result);
                index++;
            } catch (Exception ex) {
                String[] subMeasure = result.split("\\.");
                String toSek = Integer.parseInt(subMeasure[0]) * 60
                        + Integer.parseInt(subMeasure[1]) + "";
                toSek += "." + subMeasure[2];
                currentResult[index] = Double.parseDouble(toSek);
            }

        }
        playerPointsByEvents[0] = calculateTrackEventPoints(
                DecathlonEventsUtil.Run100m.A,
                DecathlonEventsUtil.Run100m.B,
                DecathlonEventsUtil.Run100m.C, currentResult[0]);
        playerPointsByEvents[1] = calculateFieldEventPoints(
                DecathlonEventsUtil.LongJump.A,
                DecathlonEventsUtil.LongJump.B,
                DecathlonEventsUtil.LongJump.C, currentResult[1]);
        playerPointsByEvents[2] = calculateFieldEventPoints(
                DecathlonEventsUtil.ShotPut.A,
                DecathlonEventsUtil.ShotPut.B,
                DecathlonEventsUtil.ShotPut.C, currentResult[2]);
        playerPointsByEvents[3] = calculateFieldEventPoints(
                DecathlonEventsUtil.HighJump.A,
                DecathlonEventsUtil.HighJump.B,
                DecathlonEventsUtil.HighJump.C, currentResult[3]);
        playerPointsByEvents[4] = calculateTrackEventPoints(
                DecathlonEventsUtil.Run400m.A,
                DecathlonEventsUtil.Run400m.B,
                DecathlonEventsUtil.Run400m.C, currentResult[4]);
        playerPointsByEvents[5] = calculateTrackEventPoints(
                DecathlonEventsUtil.Run110MHurdles.A,
                DecathlonEventsUtil.Run110MHurdles.B,
                DecathlonEventsUtil.Run110MHurdles.C, currentResult[5]);
        playerPointsByEvents[6] = calculateFieldEventPoints(
                DecathlonEventsUtil.DiscusThrow.A,
                DecathlonEventsUtil.DiscusThrow.B,
                DecathlonEventsUtil.DiscusThrow.C, currentResult[6]);
        playerPointsByEvents[7] = calculateFieldEventPoints(
                DecathlonEventsUtil.PoleVault.A,
                DecathlonEventsUtil.PoleVault.B,
                DecathlonEventsUtil.PoleVault.C, currentResult[7]);
        playerPointsByEvents[8] = calculateFieldEventPoints(
                DecathlonEventsUtil.JavelinThrow.A,
                DecathlonEventsUtil.JavelinThrow.B,
                DecathlonEventsUtil.JavelinThrow.C, currentResult[8]);
        playerPointsByEvents[9] = calculateTrackEventPoints(
                DecathlonEventsUtil.Run1500m.A,
                DecathlonEventsUtil.Run1500m.B,
                DecathlonEventsUtil.Run1500m.C, currentResult[9]);
        playerPoints.setPoints(playerPointsByEvents);
        return playerPoints;
    }

    /**
     * The Method returns an array of all persons' points to a given
     * result array.
     *
     * @param result - array of measurements of all persons.
     * @return Points[] - array of all persons' points of a given measurements
     * array.
     */
    public static final Points[] calculatePointsByPlayer(Result result) {
        int index = 0;
        Points[] pointsOfAllPlayers =
                new Points[result.getResultArray().length];
        for (String p : result.getResultArray()) {
            pointsOfAllPlayers[index] =
                    new ResultPointUtil().presetPlayerPoints(p);
            index++;
        }
        return pointsOfAllPlayers;
    }

    /**
     * Points = INT(A(B — P)C)
     * for track events(faster time produces a better score)
     * A, B and C are parameters that vary by discipline, as referred in
     * DecathlonEventsUtil.class, while P is the performance by the athlete,
     * measured in seconds (running), metres (throwing)
     * , or centimetres (jumping)
     */
    private int calculateTrackEventPoints(double a, double b, double c,
                                          double p) {
        return (int) Math.round(Math.floor(Math.pow(Math.abs(b - p), c) * a));

    }

    /**
     * INT(A(P — B)C)
     * for field events (greater distance or height produces a better score)
     * A, B and C are parameters that vary by discipline, as referred in
     * DecathlonEventsUtil.class, while P is the performance by the athlete,
     * measured in seconds (running), metres (throwing)
     * , or centimetres (jumping)
     */
    private int calculateFieldEventPoints(double a, double b, double c,
                                          double p) {
        return (int) Math.round(Math.floor(Math.pow(Math.abs(p - b), c) * a));
    }

    /**
     * The method returns a final playerPointsByEvents of all points of a
     * person (the sum of points).
     *
     * @param result - an array of all person's points of all events.
     * @return totalPlayerPoints - the sum of all person's points.
     */
    private int getTotalPointsOfPlayer(int[] result) {
        int totalPlayerPoints = 0;
        for (int i : result) {
            totalPlayerPoints += i;
        }
        return totalPlayerPoints;
    }

    /**
     * The method returns a final results of all points of all persons (the sum
     * of points).
     *
     * @param allPoints - an array of all persons' points of all events.
     * @return totalPlayerPoints[] - the sum of all persons' points of
     * all events.
     */
    public static final int[] calculateTotalPointsOfAllPlayers(
            Points[] allPoints) {
        int index = 0;
        int[] totalPointsOfAllPlayers = new int[allPoints.length];
        for (Points p : allPoints) {
            totalPointsOfAllPlayers[index] =
                    new ResultPointUtil().getTotalPointsOfPlayer(p.getPoints());
            index++;
        }
        return totalPointsOfAllPlayers;
    }

    /**
     * The method calculates the places in format of [from...to..] e.x 1-3, 2-5
     *
     * @param points - array of all persons's total points.
     * @return places[] - returns an array of assigned places, the returned
     * array is not sorted or somehow else changed in relation to a
     * given points array.
     */
    public static final String[] calculatePlaces(int[] points) {
        Set<Integer> uniquePoints = new HashSet<>();
        String[] places = new String[points.length];
        ArrayList<Integer> pointCount = new ArrayList<>();
        Map<Integer, Integer> valueList = new HashMap<>();

        // to extract unique point values from a given Point array --BEGIN
        for (int p : points) {
            uniquePoints.add(p);
        }
        // to extract unique point values from a given Point array --END

        // to sort extracted unique values in ascending manner --BEGIN
        int[] sortedPoints = new int[uniquePoints.size()];
        int index = 0;
        for (Integer p : uniquePoints) {
            sortedPoints[index] = p;
            index++;
        }
        Arrays.sort(sortedPoints);
        // to sort extracted unique values in ascending manner --END

        // to get the amount of repeated values in a given point array --BEGIN
        int counter = 0;
        for (int i = 0; i < points.length; i++) {
            for (int j = i; j < points.length; j++) {
                if (points[i] == points[j] && !pointCount.contains(points[i])) {
                    counter++;
                }
            }
            if (counter > 0) {
                valueList.put(points[i], counter);
            }

            pointCount.add(points[i]);
            counter = 0;
        }
        // to get the amount of repeated values in a given point array --END

        // to get the actual places in place-place manner --BEGIN
        int length = points.length;
        Map<Integer, String> mutualPlaces = new HashMap<>();
        int placeIndex = 0;
        int playerTotalPoints;
        while (length != 0) {
            if (placeIndex < sortedPoints.length) {
                playerTotalPoints = sortedPoints[placeIndex];
            } else {
                break;
            }
            placeIndex++;
            if (valueList.get(playerTotalPoints) == 1) {
                mutualPlaces.put(playerTotalPoints, "" + length);
                length = length - valueList.get(playerTotalPoints);
            } else {
                int change = valueList.get(playerTotalPoints) - 1;
                int result = length - change;
                mutualPlaces.put(playerTotalPoints, result + "-" + length);
                length = length - valueList.get(playerTotalPoints);
            }
        }
        // to get the actual places in place-place manner --END

        // to assign places to a given point array --BEGIN
        for (int i = 0; i < points.length; i++) {
            places[i] = mutualPlaces.get(points[i]);
        }
        // to assign places to a given point array --END
        return places;
    }

}
