package lt.swedbank.decathlon.util;

/**
 * Each event in decathlon has a threshold on which
 * points are being calculated for an achieved results.
 * More info - http://en.wikipedia.org/wiki/Decathlon
 *
 * */
public class DecathlonEventsUtil {

	/**
	 * 
	 * A class to keep 100 meters run event's parameters to calculate points.
	 * For more info http://en.wikipedia.org/wiki/Decathlon.
	 * 
	 */
	static class Run100m {
		// default values
		static final double A = 25.4347;
		static final double B = 18;
		static final double C = 1.81;
	}

	/**
	 * A class to keep long jump event's parameters to calculate points. For
	 * more info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class LongJump {
		// default values
		static final double A = 0.14354;
		static final double B = 220;
		static final double C = 1.4;
	}

	/**
	 * A class to keep shot put event's parameters to calculate points. For more
	 * info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class ShotPut {
		// default values
		static final double A = 51.39;
		static final double B = 1.5;
		static final double C = 1.05;
	}

	/**
	 * A class to keep high jump event's parameters to calculate points. For
	 * more info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class HighJump {
		// default values
		static final double A = 0.8465;
		static final double B = 75;
		static final double C = 1.42;
	}

	/**
	 * A class to keep 400 meters run event's parameters to calculate points.
	 * For more info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class Run400m {
		// default values
		static final double A = 1.53775;
		static final double B = 82;
		static final double C = 1.81;
	}

	/**
	 * A class to keep 110 meters run with hurdles event's parameters to
	 * calculate points. For more info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class Run110MHurdles {
		// default values
		static final double A = 5.74352;
		static final double B = 28.5;
		static final double C = 1.92;
	}

	/**
	 * A class to keep discus throw event's parameters to calculate points. For
	 * more info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class DiscusThrow {
		// default values
		static final double A = 12.91;
		static final double B = 4;
		static final double C = 1.1;
	}

	/**
	 * A class to keep pole vault event's parameters to calculate points. For
	 * more info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class PoleVault {
		// default values
		static final double A = 0.2797;
		static final double B = 100;
		static final double C = 1.35;
	}

	/**
	 * A class to keep javelin throw event's parameters to calculate points. For
	 * more info http://en.wikipedia.org/wiki/Decathlon.
	 */

	static class JavelinThrow {
		// default values
		static final double A = 10.14;
		static final double B = 7;
		static final double C = 1.08;
	}

	/**
	 * A class to keep 1500 meters run event's parameters to calculate points.
	 * For more info http://en.wikipedia.org/wiki/Decathlon.
	 */
	static class Run1500m {
		// default values
		static final double A = 0.03768;
		static final double B = 480;
		static final double C = 1.85;
	}

}
