package lt.swedbank.decathlon.util;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVParserUtil {

    /**
     * The method does actual parsing of a given CSV format file
     *
     * @param inputFile - file to be parsed
     * @return list of strings
     * @throws FileNotFoundException if file is missing
     */
    public static final String[] getParsedOutput(File inputFile)
            throws FileNotFoundException {
        List<String> data = new ArrayList<>();
        String currentLine;
        try(Scanner scanner = new Scanner(inputFile)) {
            while (scanner.hasNext()) {
                currentLine = scanner.nextLine();
                if (!currentLine.trim().isEmpty()) {
                    data.add(currentLine);
                }
            }
        }
        return data.toArray(new String[data.size()]);
    }

}
