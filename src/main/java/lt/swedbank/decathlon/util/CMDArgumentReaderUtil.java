package lt.swedbank.decathlon.util;

import lt.swedbank.decathlon.util.dto.InputArgs;

import java.io.File;

/**
 * Utility to help read arguments from command line
 */
public class CMDArgumentReaderUtil {
    /**
     * @param args - string array of passed arguments
     * @return InputArgs - state and input parameters
     */
    public static InputArgs readArguments(String[] args) {
        int i = 0;
        InputArgs inputArgs = new InputArgs();
        if (args[i].equals("-i") || args[i].equals("-input")) {
            i++;
            for (int j = i; j < args.length; j++) {
                if (args[i].equals("-o") || args[i].equals("-output")) {
                    break;
                } else {
                    inputArgs.setInputFile(new File(args[i]));
                    i++;
                }

            }

        }
        if (args[i].equals("-o") || args[i].equals("-output")) {
            i++;
            inputArgs.setOutputFile(new File(args[i]));
            inputArgs.setState(0);
            return inputArgs;
        }
        if (args[i].equals("-h") || args[i].equals("-help")) {
            System.out.println("-i|-input path/to/input_file");
            System.out.println("-o|-output path/to/output_file");
            System.out.println("-h|-help prints this help");
            System.out.println("NOTE: in argument line "
                    + "-i|-input option must be first.");
            inputArgs.setState(1);
            return inputArgs;
        } else {
            System.out.println("Something wrong try -h or -help option.");
            inputArgs.setState(1);
            return inputArgs;
        }
    }

}
