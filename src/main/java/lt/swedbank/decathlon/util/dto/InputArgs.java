package lt.swedbank.decathlon.util.dto;

import java.io.File;

/**
 * To keep arguments that we read from command line
 * input file  - file to read from
 * output file - file to be filed with results
 * state       - indicates if reading parameters from cmd was successful (0) or
 *               unsuccessful (1)
 *
 * */
public class InputArgs {
    private File inputFile;
    private File outputFile;
    private int state;

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
