package lt.swedbank.decathlon.domain;
/**
 * Used to keep parsed data from file as result object
 */
public class Result {
    private String[] resultArray;

    public void setResultArray(String[] parsedData) {
        int index = 0;
        resultArray = new String[parsedData.length];
        for (String p : parsedData) {
            String result;
            result = p.split(";", 2)[1];
            resultArray[index] = result;
            index++;
        }

    }

    /**
     * The method returns an array of resultString of all persons.
     *
     * @return resultArray - resultString of all persons.
     */
    public String[] getResultArray() {
        return resultArray;
    }
}
