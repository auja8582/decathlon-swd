package lt.swedbank.decathlon.domain;

/**
 * calculated outcome of achieved results(input file) in point value.
 */
public class Points {

    private int[] points;

    /**
     * The method sets points, calculated from given data string
     *
     * @param points - point array calculated for one person
     */
    public void setPoints(int[] points) {
        this.points = points;
    }

    /**
     * The method returns person's point array
     *
     * @return points
     */
    public int[] getPoints() {
        return points;
    }

}
