package lt.swedbank.decathlon;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static lt.swedbank.decathlon.util.TestUtil.removeAllSpaces;
import static org.junit.Assert.assertEquals;

public class MainTestIO {

    private PrintStream sysOut;
    private final ByteArrayOutputStream outContent =
            new ByteArrayOutputStream();


    @Before
    public void setUpStreams() {
        sysOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void revertStreams() {
        System.setOut(sysOut);
    }

    @Test
    public void testMainMethodWithValidParameterH()
            throws IOException {

        String[] args = new String[1];
        args[0] = "-h";
        Main.main(args);
        assertEquals(removeAllSpaces(outContent.toString()), removeAllSpaces(
                "-i|-input path/to/input_file\n" +
                        "-o|-output path/to/output_file\n" +
                        "-h|-help prints this help\n" +
                        "NOTE: in argument line -i" +
                        "|-input option must be first."));
    }

    @Test
    public void testMainMethodWithValidParameterHelp()
            throws IOException {

        String[] args = new String[1];
        args[0] = "-help";
        Main.main(args);
        assertEquals(removeAllSpaces(outContent.toString()), removeAllSpaces(
                "-i|-input path/to/input_file\n" +
                        "-o|-output path/to/output_file\n" +
                        "-h|-help prints this help\n" +
                        "NOTE: in argument line -i" +
                        "|-input option must be first."));
    }

}
