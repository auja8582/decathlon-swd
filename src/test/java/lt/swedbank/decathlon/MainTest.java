package lt.swedbank.decathlon;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import static lt.swedbank.decathlon.util.TestUtil.*;
import static org.junit.Assert.*;

public class MainTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @After
    public void deleteTestFolder(){
        testFolder.delete();
    }

    @Test
    public void testMainMethodWithValidParameters()
            throws IOException, URISyntaxException {

        String[] args = new String[4];
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "inputFiles/Decathlon_input0.txt";
        String outputFilePath = testFolder
                .newFile("Decathlon_output0.txt").getAbsolutePath();
        String verifiedOutputFilePath = "verifiedOutputFiles" +
                "/Decathlon_output0_verified.xml";

        args[0] = "-i";
        args[1] = getAbsolutePath(inputFilePath, classLoader);
        args[2] = "-o";
        args[3] = outputFilePath;

        Main.main(args);

        String output = convertStreamToString(new FileInputStream(
                new File(outputFilePath)));

        String verifiedOutput = convertStreamToString(classLoader
                .getResourceAsStream(verifiedOutputFilePath));

        assertEquals(removeAllSpaces(output), removeAllSpaces(verifiedOutput));
    }

    @Test
    public void testMainMethodWithValidParametersFullNames()
            throws IOException, URISyntaxException {

        String[] args = new String[4];
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "inputFiles/Decathlon_input0.txt";
        String outputFilePath = testFolder
                .newFile("Decathlon_output0.txt").getAbsolutePath();
        String verifiedOutputFilePath = "verifiedOutputFiles" +
                "/Decathlon_output0_verified.xml";

        args[0] = "-input";
        args[1] = getAbsolutePath(inputFilePath, classLoader);
        args[2] = "-output";
        args[3] = outputFilePath;

        Main.main(args);

        String output = convertStreamToString(new FileInputStream(
                new File(outputFilePath)));

        String verifiedOutput = convertStreamToString(classLoader
                .getResourceAsStream(verifiedOutputFilePath));

        assertEquals(removeAllSpaces(output), removeAllSpaces(verifiedOutput));
    }

    @Test
    public void testMainMethodWithValidParametersAndOnePersonPerPlace()
            throws IOException, URISyntaxException {

        String[] args = new String[4];
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "inputFiles/Decathlon_input1.txt";
        String outputFilePath = testFolder
                .newFile("Decathlon_output1.txt").getAbsolutePath();
        String verifiedOutputFilePath = "verifiedOutputFiles" +
                "/Decathlon_output1_verified.xml";

        args[0] = "-input";
        args[1] = getAbsolutePath(inputFilePath, classLoader);
        args[2] = "-output";
        args[3] = outputFilePath;

        Main.main(args);

        String output = convertStreamToString(new FileInputStream(
                new File(outputFilePath)));

        String verifiedOutput = convertStreamToString(classLoader
                .getResourceAsStream(verifiedOutputFilePath));

        assertEquals(removeAllSpaces(output), removeAllSpaces(verifiedOutput));
    }

    @Test
    public void testMainMethodWithValidParametersAndSeveralPersonsPerPlace()
            throws IOException, URISyntaxException {

        String[] args = new String[4];
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "inputFiles/Decathlon_input2.txt";
        String outputFilePath = testFolder
                .newFile("Decathlon_output2.txt").getAbsolutePath();
        String verifiedOutputFilePath = "verifiedOutputFiles" +
                "/Decathlon_output2_verified.xml";

        args[0] = "-input";
        args[1] = getAbsolutePath(inputFilePath, classLoader);
        args[2] = "-output";
        args[3] = outputFilePath;

        Main.main(args);

        String output = convertStreamToString(new FileInputStream(
                new File(outputFilePath)));

        String verifiedOutput = convertStreamToString(classLoader
                .getResourceAsStream(verifiedOutputFilePath));

        assertEquals(removeAllSpaces(output), removeAllSpaces(verifiedOutput));
    }

    @Test
    public void testMainMethodWithInvalidParametersAndSeveralPersonsPerPlace()
            throws IOException, URISyntaxException {

        String[] args = new String[4];
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "inputFiles/Decathlon_input3.txt";
        String outputFilePath = testFolder
                .newFile("Decathlon_output3.txt").getAbsolutePath();
        Exception ex = null;

        args[0] = "-input";
        args[1] = getAbsolutePath(inputFilePath, classLoader);
        args[2] = "-output";
        args[3] = outputFilePath;
        try {
            Main.main(args);
            fail("fail");
        } catch (NumberFormatException e){
            ex = e;
        }
        assertTrue(ex instanceof NumberFormatException);
    }

}
