# README #

The aftermath:

applied for junior java developer position at Swedbank AB and was given to do this task 
as a pre interview task. Was told that my programming skills suck and that I'm a total shit.

### TASK: ###

The task is about Decathlon competition.

The input of the Java program is a CSV-like text file (see the attachment). 
The task is to output an XML file with all athletes in ascending order of their places, 
containing all the input data plus total score and the place in the competition 
(in case of equal scores, athletes must share the places, e.g. 3-4 and 3-4 instead of 3
and 4). Input and output file names should be provided as parameters to the Java 
application at the startup. Application should ask for those parameters itself.

It would be great if an XSL file for viewing the produced XML nicely using 
a web browser is also provided.

Be sure to keep the code design simple, but allowing to easily change or add 
more input sources and/or output file formats.

Unit tests for the code are mandatory.

No external libraries are allowed in addition to the Java standard API except 
JUnit.

Gradle should be used as building tool.

Recommended java version is 7, but not mandatory.

Please zip your code together with the project file. Keep in mind that we are 
going to run both your program and the tests.  
It is preferred that IntelliJ is used as IDEA.

Some links:

http://en.wikipedia.org/wiki/Decathlon (see formulas are at the end of the page)

http://www.junit.org/ - JUnit

http://www.jetbrains.com/ - IntelliJ IDEA download

Try to keep your code as readable as possible. We value code simplicity. 
Use object-oriented approach with design patterns where applicable.

Good luck!

### What is this repository for? ###

This repository is for swedbank's given task - decathlon - source management

### How do I get set up? ###
 1. clone  https://auja8582@bitbucket.org/auja8582/decathlon-swd.git
 
    OR
 
    File -> New -> Project from Version Control and use 
    https://auja8582@bitbucket.org/auja8582/decathlon-swd.git url to clone project (intellij)
 
    Note: project uses gradle as project management tool

2. Build jar with gradle:

   > Tasks -> Build -> jar (intellij gradle plugin)

3. Go to path/to/project/decathlon-swd/build

4. Copy the jar file from path/to/project/decathlon-swd/build and put into directory you like

5. Copy xsl file from path/to/project/decathlon-swd/src/main/resources/xslt_file.xsl

6. Put xslt_file.xsl file to directory where you placed jar file 

7. Execute jar from comand line tool as

   > java -jar pat/to/jar/jar_file.jar -i path/to/input/file/Decathlon_input.txt -o path/to/output/file.xml

8. To view output xml file with xsl file, output file and xsl file must be in the same directory, 
   open output.xml file in internet explorer
   
9. To execute tests   
   Tasks -> verifcation -> test (intellij gradle plugin)

10. Project uses jdk 8, however should be compatible with jdk 7 (not tested)
### What is missing, and sloppy edges, TODO ###

1. Not all classes have test cases, only business logic in Main cass.
2. APPLICATION DOES NOT ASK FOR PARAMETERS TO INPUT, I guess I was to 
   lazy to implement this interactive option.
3. It is perfect that jUnit was allowed in project, however to use log4j,
   slf4j would also be a HUGE asset in the project, using System.out.print(ln) is not very portable.